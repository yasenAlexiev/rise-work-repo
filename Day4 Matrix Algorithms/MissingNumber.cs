﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4_Matrix_Algorithms
{
    public class MissingNumber
    {
        public static int FindMissing(int[] num_array)
        {
            int sum_missing = 0, max = num_array[0], min = num_array[0];
            for (int i = 0; i < num_array.Length; i++)
            {
                sum_missing += num_array[i];
                if (num_array[i] < min)min = num_array[i];
                if (num_array[i] > max) max = num_array[i];    
            }

            int sum_ful = 0; // use the formula for sum of N consecutive elements
            for (int i = min; i <= max; i++)
            {
                sum_ful += i;
            }
            if(sum_ful-sum_missing == 0)throw new Exception(message:"No missing number");
            return sum_ful - sum_missing;
           
        }

    }
}
